#include "pixel.hpp"

Pixel::Pixel() {}

Pixel::Pixel(int r, int g, int b)
{
  setPixelR(r);
  setPixelG(g);
  setPixelB(b);
}

void Pixel::setPixel(int r, int g, int b)
{
  setPixelR(r);
  setPixelG(g);
  setPixelB(b);
}

void Pixel::setPixelR(int r)
{
  this->r = r;
}
int Pixel::getPixelR()
{
    return this->r;
}

void Pixel::setPixelG(int g)
{
    this->g = g;
}

int Pixel::getPixelG()
{
  return this->g;
}

void Pixel::setPixelB(int b)
{
  this->b = b;
}

int Pixel::getPixelB()
{
    return this->b;
}
