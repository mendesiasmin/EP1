#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "pixel.hpp"

using namespace std;

class Image: public Pixel{
  private:
    string type;
    string name;
    int scale;
    int image_width;
    int image_height;

  protected:
    Pixel **image;

  public:
		Image();
		Image(string origin);
		void setType(string type);
		string getType();
		void setName(string name);
		string getName();
		void setScale(int scale);
		int getScale();
		void setWidth(int width);
		int getWidth();
		void setHeight(int height);
		int getHeight();
    void setImage(string origin);
    void saveImage();
    Pixel** allocation_matrix();
};
#endif
