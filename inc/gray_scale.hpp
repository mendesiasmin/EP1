#ifndef GRAY_SCALE_HPP
#define GRAY_SCALE_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "pixel.hpp"
#include "image.hpp"

using namespace std;

class GrayScale: public Image {

  private:
    Pixel **gray_scale_image;

  public:
    GrayScale(string origin);
    GrayScale(string origin, string name);
    void apply_filter();
    void saveImage();
    void saveImage(string name);
};

#endif
